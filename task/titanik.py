import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    grouped = df.groupby(df['Name'].str.extract("([A-Za-z]+)\.", expand=False))
    results = []
    new_list = []

    for title, group in grouped:
        missing_values = group['Age'].isnull().sum()
        median_value = round(group['Age'].median())
        results.append((title + ".", missing_values, median_value))

    new_list.append(results[12])
    new_list.append(results[13])
    new_list.append(results[9])
    return new_list
